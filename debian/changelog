libgarmin (0~svn320-7) UNRELEASED; urgency=medium

  * d/changelog: Remove trailing whitespaces

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 10:21:27 +0200

libgarmin (0~svn320-6) unstable; urgency=medium

  * debian/control: make Vcs-* fields point to salsa.debian.org

 -- Gilles Filippini <pini@debian.org>  Sun, 18 Feb 2018 18:35:24 +0100

libgarmin (0~svn320-5) unstable; urgency=medium

  [ Helmut Grohne ]
  [Helmut Grohne <helmut@subdivi.de>]
  * Fix FTCBFS: Let dh_auto_configure pass --host to ./configure
    (closes: #882922)

  [ Gilles Filippini ]
  * debian/copyright: update to format version 1.0
  * Bump dh compat level to 10
  * Multiarch support
  * debian/control:
    - Use secure URIs for Vcs-* fields
    - Priority: optional (instead of extra)
    - Bump Standards-Version to 4.1.1
  * update debian/watch and debian/README.source
  * Remove useless debian/*.dirs

 -- Gilles Filippini <pini@debian.org>  Tue, 28 Nov 2017 21:37:23 +0100

libgarmin (0~svn320-4) unstable; urgency=low

  * New patch: automake
    Fix FTBFS due to automake warnings treated as errors (closes: #713290)
  * Fix VCS fields in debian/control
  * Force build flags from dpkg-buildflags in debian/rules

 -- Gilles Filippini <pini@debian.org>  Sat, 13 Jul 2013 20:11:13 +0200

libgarmin (0~svn320-3) unstable; urgency=low

  * New patch move-fcntl-before-libgarmin_priv to fix FTBFS on kfreebsd-*

 -- Gilles Filippini <pini@debian.org>  Sat, 09 Apr 2011 00:42:06 +0200

libgarmin (0~svn320-2) unstable; urgency=low

  * debian/libgarmin-dev.install
    + Remove libgarmin.la (closes: #621614)
  * debian/control:
    + Updated Maintainer field with my @d.o address
    + Bumped Standards-Version to 3.9.2 (no other changes)
  * debian/rules:
    + Migrated to the dh sequencer
    + Migrated to source format 3.0 (quilt)
  * Updated debian/README.source

 -- Gilles Filippini <pini@debian.org>  Fri, 08 Apr 2011 22:27:01 +0200

libgarmin (0~svn320-1) unstable; urgency=low

  * Initial release (Closes: #521466)

 -- Gilles Filippini <gilles.filippini@free.fr>  Mon, 18 May 2009 13:55:37 +0200
