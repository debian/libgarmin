This file is used to keep track of packaging choices, known issues and TO-DOs.

==================================================
1- HOW THE SOURCE PACKAGE IS HANDLED USING SVN/GIT
==================================================

The work on this package takes place on the alioth git repository.
It requires an alioth account with grant to the collab-maint group.

Cloning the repository:

 $ git clone <my-alioth-login>@alioth.debian.org:/git/collab-maint/libgarmin.git

The repository has 4 remote branches:
* origin/upstream-svn
* origin/upstream
* origin/master
* origin/pristine-tar

HEAD is binded to master. 
master fetched localy by the clone:

 $ git branch
 * master

A daily cronjob syncs origin/upstream-svn with upstream's SVN repository
(<http://libgarmin.svn.sourceforge.net/svnroot/libgarmin/libgarmin/dev>).

A packaging cycle starts by merging origin/upstream-svn with upstream. It
requires having checked out origin/upstream localy (need to be done once):

 $ git checkout -b upstream origin/upstream

Then we can do the merge:

 $ git merge origin/upstream-svn

It can leads to conflicts when parts of pristine upstream tree have been
removed in our upstream branch.
As of today no part of pristine upstream has been removed. 

When the merge is completed and committed we then switch to the master branch
to merge upstream:

  $ git checkout master
  $ git merge upstream

Then we can work on the packaging, still in the master branch.

See below how to patch upstream source code.

The package is built using git-buildpackage. See
<http://honk.sigxcpu.org/projects/git-buildpackage/manual-html/gbp.html>

To build using cowbuilder consider using git-pbuilder

$ DIST=sid git-buildpackage --git-builder=/usr/bin/git-pbuilder

Warning: when modifying upstream branch (such as removing parts) you'll need
to rm .orig.tar.gz before building the package again.
